"use strict";
var fb = require('./fbs');
var OAuth2 = require('oauth2').OAuth2;
var oauth2 = new OAuth2("757203371154308",
  "0ff3adb79309e0c85a16b1da32a8971c",
  "", "https://www.facebook.com/dialog/oauth",
  "https://graph.facebook.com/oauth/access_token",
  null);
const request = require("request");
const express = require("express");
const bodyParser = require("body-parser");
var unirest = require("unirest");
var Cloudant = require('cloudant');
var me = "00a5c0f3-366e-428e-8009-09b9977bd7ea-bluemix"; // Set this to your own account
var password = "5b8498c9771ca2161b66e60fe2bfe44b59d7d6c821123b005c50c59ea0cc6f08";

// Initialize the library with my account.
var cloudant = Cloudant({
  account: me,
  password: password
});
var db = cloudant.db.use('query-demo')

cloudant.db.list(function (err, allDbs) {
  console.log('Available Databases: %s', allDbs.join(', '))
});
const restService = express();

restService.use(
  bodyParser.urlencoded({
    extended: true
  })
);


restService.use(bodyParser.json());


restService.post("/webhookcall", function (req, res) {
  const fbid = req.body.originalRequest.data.sender.id;


  //	console.log(req.body)
  //console.log(req.body);
  const action = req.body.result.action;
  const parameters = req.body.result.parameters;

  console.log(req.body.originalRequest.data.message);
  console.log('Server webhook data');


  if (action == 'input.savedonordata' && parameters.donor_location !='' && parameters.blood_group!='' && parameters.donor_acceptance !='' && parameters.donor_email !='') {
    console.log(req.body.originalRequest.data.sender);
    //posting Donor-data to cloudant start

    db.insert({
      _id: req.body.originalRequest.data.sender.id,
      dname: parameters.donor_name,
      dgroup: parameters.blood_group,
      dloc: parameters.donor_location,
      dacceptance: parameters.donor_acceptance,
      dcontact: parameters.donor_contact,
      dmail: parameters.donor_email,
      ddate: parameters.donation_date,
      dfbid: req.body.originalRequest.data.sender.id,
      donor_status: "active"
    }, function (err, body, header) {
      if (err) {
        return console.log('[db.insert] ', err.message);
      }

      console.log('You have inserted the Donor Details');
      console.log(body);
    });


    //posting data to cloudant end
    return res.json({
      speech: 'Your Donation details has been saved successfully',
      displayText: 'Your Donation details has been saved successfully using webhook',
      source: "bloodbank"
    });
  }
  if (action == 'input.send') {
    fb.sendbuttons(req.body.originalRequest.data.sender.id, "Process my request towards the id " + req.body.originalRequest.data.sender.id,hi);
  }
  if (action == 'deletedonor') {

    db.get(fbid, (err, data) => {
      if (err) {
        throw err;
      }
      
      let doc = data;
      doc.donor_status= "inactive";
      
      // Now mark it as updatable.
      doc.c = true; // c stands for canChange
      // Now insert it
      db.insert(doc, (err2, data2) => {
        if (err2) {
          throw err2;
        }
        console.log(data2);
      });
    });
    fb.textmessage(req.body.originalRequest.data.sender.id, "We have removed your Donation Services,Please add as a donor if you want to save a LIFE !!");
  }

  if (action == 'checkdonor') {
    db.find({
      selector: {
        dfbid: fbid
      }
    }, function (er, result) {
      if (er) {
        throw er;
      }

      console.log('Found %d documents with name db', result.docs.length);
      if (result.docs.length < 1) {
        fb.adddonor(req.body.originalRequest.data.sender.id, "I would like to be added as a donor");
        
      }
      else if(result.docs[0].donor_status == "active"){
        fb.textmessage(req.body.originalRequest.data.sender.id, "You have already Registered as a Donor");
        
      }
      else
      {
        db.get(fbid, (err, data) => {
          if (err) {
            throw err;
          }
          
          let doc = data;
          doc.donor_status= "active";
          
          // Now mark it as updatable.
          doc.c = true; // c stands for canChange
          // Now insert it
          db.insert(doc, (err2, data2) => {
            if (err2) {
              throw err2;
            }
            console.log(data2);
          });
        });
        
        fb.textmessage(req.body.originalRequest.data.sender.id, "Welcome back "+result.docs[0].dname+" We have Updated your donation status to be active");
        
      }


    });
  }


  if (action == 'input.test') {
    console.log(req.body.originalRequest.data.message.text);
//console.log(req.body.originalRequest.data.);
    console.log(parameters.requesterid);
    if (req.body.originalRequest.data.message.text == 'Yes') {
      db.find({
        selector: {
          dfbid: fbid
        }
      }, function (er, result) {
        if (er) {
          throw er;
        }

        console.log('Found %d documents with name db', result.docs.length);
        for (var i = 0; i < result.docs.length; i++) {
          console.log('  Doc loc: %s', result.docs[i].location);
          fb.textmessage(parameters.requesterid, "These are the Donor  Details \n Name: " + result.docs[i].dname+'\n Contact: '+ result.docs[i].dcontact);

        }
        if (result.docs.length < 1) {

          return res.json({
            speech: 'No donors are available at the instant',
            displayText: 'Your input name is updated',
            source: "bloodbank"
          });
        }


      });


    } else if (req.body.originalRequest.data.message.text == 'No') {

      fb.textmessage(fbid, "We feel sorry that you are unable to Donate, It's ok may you could help next time");
    }

  }


  if (action == 'input.saverecipientdata') {
    console.log('Saving the blood request in Database');
    //posting recipient-data-data to cloudant start
    console.log(req.body.originalRequest.data.sender);
    //posting Donor-data to cloudant start

    db.insert({
      rname: parameters.name,
      rgroup: parameters.blood_group,
      rloc: parameters.rlocation,
      rcontact: parameters.contact,
      fbid: req.body.originalRequest.data.sender.id
    }, function (err, body, header) {
      if (err) {
        return console.log('[db.insert] ', err.message);
      }

      console.log('You have inserted recipient  Data');
      console.log(body);
    });


    //sending message to donors
    fb.textmessage(fbid, "Please be patient while we search donors for you.")
    
    console.log('Sending message to donors');
    console.log(req.body.originalRequest.data.sender.id)
    db.find({
      selector: {
        dgroup: parameters.blood_group,
        donor_status: "active"
      }
    }, function (er, result) {
      if (er) {
        throw er;
      }

      console.log('Found %d documents with name db', result.docs.length);
      for (var i = 0; i < result.docs.length; i++) {
        console.log('  Doc loc: %s', result.docs[i].location);
        if(result.docs[i].dfbid != fbid){
        fb.sendbuttons(result.docs[i].dfbid, "Process my request towards the id " + fbid, "\nThe below person Needs Blood Name " + parameters.name + '\nContact: ' + parameters.contact+'\n Please Hit yes if you are willing to Donate');
        }
      }
      if (result.docs.length < 1) {

        return res.json({
          speech: 'No donors are available at the instant',
          displayText: 'Your input name is updated',
          source: "bloodbank"
        });
      }


    });
    //posting data to cloudant end
    /*return res.json({
      speech: 'We have registered your request. Please be patient while we consult the available donors for you.',
      displayText: 'Your Donation details has been saved successfully using webhook',
      source: "bloodbank"
    });*/


  }

  if (action == 'input.trackdonor') {
    console.log('Tracking Donor\'s Location');


    return res.json({
      speech: 'We have registered your request. Please be patient while we consult the available donors for you.',
      displayText: 'This is the current Donor\'s Location',
      source: "bloodbank"
    });
  }
  if (action == 'input.deletedonor') {
    console.log('Deleting Donor');


    return res.json({
      speech: 'We have registered your request. Please be patient while we consult the available donors for you.',
      displayText: 'Your Donor Details have deleted using Webhook',
      source: "bloodbank"
    });
  }
  if (action == 'input.donationstatus') {
    console.log('Getting the Donoation details');


    return res.json({
      speech: 'We have registered your request. Please be patient while we consult the available donors for you.',
      displayText: 'These are your donation details',
      source: "bloodbank"
    });
  }

  if (action == 'input.name') {
    console.log('updating name');


    return res.json({
      speech: 'We have registered your request. Please be patient while we consult the available donors for you.',
      displayText: 'Your input name is updated',
      source: "bloodbank"
    });
  }


  if (action == 'input.getdonors') {
    console.log('getting donors');
    console.log(req.body.originalRequest.data.sender.id)

    db.find({
      selector: {
        dgroup: parameters.bgroup
      }
    }, function (er, result) {
      if (er) {
        throw er;
      }

      console.log('Found %d documents with name db', result.docs.length);
      for (var i = 0; i < result.docs.length; i++) {
        console.log('  Doc loc: %s', result.docs[i].location);
        fb.textmessage(req.body.originalRequest.data.sender.id, "\nDonor Name:" + result.docs[i].dname + '\nContact: ' + result.docs[i].dcontact)

      }
      if (result.docs.length < 1) {

        return res.json({
          speech: 'No donors are available at the instant',
          displayText: 'Your input name is updated',
          source: "bloodbank"
        });
      }


    });

  }
  if (action == 'getdetails') {
    db.find({
      selector: {
        dfbid: fbid
      }
    }, function (er, result) {
      if (er) {
        throw er;
      }

      console.log('Found %d documents with name db', result.docs.length);
      for (var i = 0; i < result.docs.length; i++) {
        console.log('  Doc loc: %s', result.docs[i].location);
        fb.textmessage(fbid, "\nDonor Name:" + result.docs[i].dname + '\nContact: ' + result.docs[i].dcontact+'\nEmail'+result.docs[i].dmail+'\nBlood Group: '+result.docs[i].dgroup+'\nPincode: '+result.docs[i].dloc)

      }
      if (result.docs.length < 1) {
      
        fb.adddonor(fbid, "I would like to be added as a donor");
        
      }


    });

    }


});


restService.listen(process.env.PORT || 8080, function () {
  console.log("Server up and listening");
});