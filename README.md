# Cognitive  Blood Bank using Google's Dialog Flow


### WARNING:
This application is running in IBM Git Pipeline kindly check before making any changes to the master. Creating a branch to make any further enhancements is preferable

### Note:
Before starting with the Project, please note that due to cambridge Analytica's Facebook breach, facebook is not allowing developer integrations for facebook at certain times.

## Front End

For this chatbot Google's api.ai has been used as front End. Using Dialog Flows one click integration service we have integrated this chatbot to facebook's developers page by creating an application.

## Clone the Project

After cloning the project install all the dependancies into your machine

    $ npm install
    
    
## Create you own Dialog Flow Account

Now, create a project with name Blood Bank and upload the JSON File in the DialogFlow Folder into the your workspace using import options in the settings Icon. 


## One Click Integration

Integrate your chatbot to the facebook page using One Click Integration

## Download NGRok

Download latest version NGRoK for the Tunelling system for the temporary server to use webhook services. After downloading run the NGRok and copy the url into the Fulfilment after enabling them.

## Configure your Cloudant Credentials

Replace the cloudant credentials with your own cloudant credentails or you can still continue with the existing cloudant credentials which are live and running.

## Run the Server

Once all the dependancies are installed now run the application in the root directory

    $ node app.js