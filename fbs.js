const request = require('request');
var events = [
    {
        "name": "hi"
    }
]
module.exports.sendbuttons = function (fbid,text,data) {
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: "EAAKwrCX124QBAFZBBaKwQafyQl5u1EK2WI7FXvhbUJQg9PU2imTuVZCdGgT1ZAZClMZCgfyYMTLG8JaPZBMVbRq4zxj84IJM6VNUuY7khZCNpj5LRbZAPn092p2yBAPUmyIE22eC0bWTZBMHbmnM5YeZA3d7vvEOJUn6GxLmaCTWoM2YqjipvQIIie"
        },
        method: 'POST',
        json: {
            recipient: {
                id: fbid
            },
            "message":{
                "text": data,
                "quick_replies":[
                  {
                    "content_type":"text",
                    "title":"Yes",
                    "payload": text
                  },
                  {
                    "content_type":"text",
                    "title":"No",
                    "payload":text
                  }
                ]
              }
        }
    }, (error, response) => {
        if (error) {
            console.log('Error sending message: ', error);
        } else if (response.body.error) {
            console.log('Error: ', response.body.error);
        }
    });
}


module.exports.textmessage = function (sender, aiText) {
   request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: "EAAKwrCX124QBAFZBBaKwQafyQl5u1EK2WI7FXvhbUJQg9PU2imTuVZCdGgT1ZAZClMZCgfyYMTLG8JaPZBMVbRq4zxj84IJM6VNUuY7khZCNpj5LRbZAPn092p2yBAPUmyIE22eC0bWTZBMHbmnM5YeZA3d7vvEOJUn6GxLmaCTWoM2YqjipvQIIie"
        },
        method: 'POST',
        json: {
            recipient: {
                id: sender
            },
            message: {
                text: aiText
            }
        }
    }, (error, response) => {
        if (error) {
            console.log('Error sending message: ', error);
        } else if (response.body.error) {
            console.log('Error: ', response.body.error);
        }
    });
}


module.exports.senda = function (sender, pl) {
    var pl1=pl.toString()+'1',pl2=pl.toString()+'2',pl3=pl.toString()+'3';
    console.log(pl1,pl2,pl3);
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: "EAAKwrCX124QBAFZBBaKwQafyQl5u1EK2WI7FXvhbUJQg9PU2imTuVZCdGgT1ZAZClMZCgfyYMTLG8JaPZBMVbRq4zxj84IJM6VNUuY7khZCNpj5LRbZAPn092p2yBAPUmyIE22eC0bWTZBMHbmnM5YeZA3d7vvEOJUn6GxLmaCTWoM2YqjipvQIIie"
        },
        method: 'POST',
        json: {
            recipient: {
                id: sender
            },
            message: {
                attachment: {
                    type: 'template',
                    payload: {
                        template_type: 'button',
                        text: 'select',
                        buttons: [
           
                            {
                                type: 'postback',
                                title: 'Yes',
                                payload: pl1
          },
                                                    {
                                type: 'postback',
                                title: 'Cancel',
                                payload: pl2
          },
                            {
                                type: 'postback',
                                title: 'No',
                                payload: pl3
          }
        ]
                       
    
        
                    }
                }
            }
        }
    }, (error, response) => {
        if (error) {
            console.log('Error sending message: ', error);
        } else if (response.body.error) {
            console.log('Error: ', response.body.error);
        }
    });
}
module.exports.adddonor = function (sender,text) {
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: "EAAKwrCX124QBAFZBBaKwQafyQl5u1EK2WI7FXvhbUJQg9PU2imTuVZCdGgT1ZAZClMZCgfyYMTLG8JaPZBMVbRq4zxj84IJM6VNUuY7khZCNpj5LRbZAPn092p2yBAPUmyIE22eC0bWTZBMHbmnM5YeZA3d7vvEOJUn6GxLmaCTWoM2YqjipvQIIie"
        },
        method: 'POST',
        json: {
            recipient: {
                id: sender
            },
            "message":{
                "text": "Want to be a Donor and Save a LIFE !, Please Click Yes if you can Donate",
                "quick_replies":[
                  {
                    "content_type":"text",
                    "title":"Yes",
                    "payload": text
                  },
                  {
                    "content_type":"text",
                    "title":"No",
                    "payload":"I don't want to be a Donor"
                  }
                ]
              }
        }
    }, (error, response) => {
        if (error) {
            console.log('Error sending message: ', error);
        } else if (response.body.error) {
            console.log('Error: ', response.body.error);
        }
    });
}